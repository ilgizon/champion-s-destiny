package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentAbilitiesBinding
import com.champions.destiny.app.databinding.FragmentChampionBinding

class AbilitiesFragment : Fragment() {

    private var _binding: FragmentAbilitiesBinding? = null
    private val binding: FragmentAbilitiesBinding
        get() = _binding!!
    private var btnBackClicked = false
    private var btnMenuClicked = false
    private var btnNextClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAbilitiesBinding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                MAIN.navController.navigate(R.id.action_abilitiesFragment_to_championFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        binding.backBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnBackClicked){
                btnBackClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_abilitiesFragment_to_championFragment)
                }, 200)
            }
        }
        binding.menuBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnMenuClicked){
                btnMenuClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_abilitiesFragment_to_mainMenuFragment)
                }, 200)
            }
        }
        binding.nextBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnNextClicked){
                btnNextClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_abilitiesFragment_to_infoFragment)
                }, 200)
            }
        }
        val champion = sharedPreferences.getString("champion", "true")
        when (champion) {
            "jinx" -> {
                setJinxAbilities()
            }
            "garen" -> {
                setGarenAbilities()
            }
            "katarina" -> {
                setKatarinaAbilities()
            }
            "yasuo" -> {
                setYasuoAbilities()
            }
            "ashe" -> {
                setAsheAbilities()
            }
            "miss_fortune" -> {
                setMissFortuneAbilities()
            }
            "darius" -> {
                setDariusAbilities()
            }
        }
    }

    private fun setJinxAbilities(){
        binding.imageView.setImageResource(R.drawable.jinx_passive_img)
        binding.ability1.text = "GET EXCITED!\nJinx receives massively increased Move Speed and Attack Speed whenever she helps kill or destroy an enemy champions epic jungle monster, or structure."

        binding.imageView2.setImageResource(R.drawable.jinx_q)
        binding.ability2.text = "SWITCHEROO!\nJinx modifies her basic attacks by swapping between Pow-Pow, her minigun and Fishbones, her rocket launcher. Attacks with Pow-Pow grant Attack Speed, while attacks with Fishbones deal area of effect damage, gain increased range, but drain Mana and attack slower."

        binding.imageView3.setImageResource(R.drawable.jinx_w)
        binding.ability3.text = "ZAP!\nJinx uses Zapper, her shock pistol, to fire a blast that deals damage to the first enemy hit, slowing and revealing it."

        binding.imageView4.setImageResource(R.drawable.jinx_e)
        binding.ability4.text = "FLAME CHOMPERS!\nJinx throws out a line of snare grenades that explode after 5 seconds, lighting enemies on fire. Flame Chompers will bite enemy champions who walk over them, rooting them in place."

        binding.imageView5.setImageResource(R.drawable.jinx_r)
        binding.ability5.text = "SUPER MEGA DEATH ROCKET!\nJinx fires a super rocket across the map that gains damage as it travels. The rocket will explode upon colliding with an enemy champion, dealing damage to it and surrounding enemies based on their missing Health."
    }

    private fun setGarenAbilities() {
        binding.imageView.setImageResource(R.drawable.garen_passive)
        binding.ability1.text = "PERSEVERANCE\nIf Garen has not recently been struck by damage or enemy abilities, he regenerates a percentage of his total health each second."

        binding.imageView2.setImageResource(R.drawable.garen_q)
        binding.ability2.text = "DECISIVE STRIKE\nGaren gains a burst of Move Speed, breaking free of all slows affecting him. His next attack strikes a vital area of his foe, dealing bonus damage and silencing them."

        binding.imageView3.setImageResource(R.drawable.garen_w)
        binding.ability3.text = "COURAGE\nGaren passively increases his armor and magic resist by killing enemies. He may also activate this ability to give him a shield and tenacity for a brief moment followed by a lesser amount of damage reduction for a longer duration."

        binding.imageView4.setImageResource(R.drawable.garen_e)
        binding.ability4.text = "JUDGMENT\nGaren rapidly spins his sword around his body, dealing physical damage to nearby enemies."

        binding.imageView5.setImageResource(R.drawable.garen_r)
        binding.ability5.text = "DEMACIAN JUSTICE\nGaren calls upon the might of Demacia to attempt to execute an enemy champion."
    }

    private fun setKatarinaAbilities() {
        binding.imageView.setImageResource(R.drawable.katarina_passive)
        binding.ability1.text = "VORACITY\nWhenever an enemy champion dies that Katarina has damaged recently, her remaining ability cooldowns are dramatically reduced. If Katarina picks up a Dagger, she uses it to slash through all nearby enemies, dealing magic damage."

        binding.imageView2.setImageResource(R.drawable.katarina_q)
        binding.ability2.text = "BOUNCING BLADE\nKatarina throws a Dagger at the target that then bounces to nearby enemies before ricocheting onto the ground."

        binding.imageView3.setImageResource(R.drawable.katarina_w)
        binding.ability3.text = "PREPARATION\nKatarina gains a burst of Move Speed, tossing a Dagger into the air directly above herself."

        binding.imageView4.setImageResource(R.drawable.katarina_e)
        binding.ability4.text = "SHUNPO\nKatarina blinks to the target, striking it if its an enemy, or striking the nearest enemy otherwise."

        binding.imageView5.setImageResource(R.drawable.katarina_r)
        binding.ability5.text = "DEATH LOTUS\nKatarina becomes a flurry of blades, dealing massive magic damage while she channels to the 3 nearest enemy champions."
    }

    private fun setYasuoAbilities() {
        binding.imageView.setImageResource(R.drawable.yasuo_passive)
        binding.ability1.text = "WAY OF THE WANDERER\nYasuo's Critical Strike Chance is increased. Additionally, Yasuo builds toward a shield whenever he is moving. The shield triggers when he takes damage from a champion or monster."

        binding.imageView2.setImageResource(R.drawable.yasuo_q)
        binding.ability2.text = "STEEL TEMPEST\nThrusts forward, damaging all enemies in a line. On hit, grants a stack of Gathering Storm for a few seconds. At 2 stacks, Steel Tempest fires a whirlwind that knocks Airborne. Steel Tempest is treated as a basic attack and scales with all the same things."

        binding.imageView3.setImageResource(R.drawable.yasuo_w)
        binding.ability3.text = "WIND WALL\nCreates a moving wall that blocks all enemy projectiles for 4 seconds."

        binding.imageView4.setImageResource(R.drawable.yasuo_e)
        binding.ability4.text = "SWEEPING BLADE\nDashes through target enemy, dealing magic damage. Each cast increases subsequent dash's damage, up to a max amount. Cannot be re-cast on the same enemy for a few seconds. If Steel Tempest is cast while dashing, it will strike as a circle."

        binding.imageView5.setImageResource(R.drawable.yasuo_r)
        binding.ability5.text = "LAST BREATH\nBlinks to an Airborne enemy champion, dealing physical damage and holding all Airborne enemies in the area in the air. Grants maximum Flow but resets all stacks of Gathering Storm. For a moderate time afterwards, Yasuo's critical strikes gain significant Bonus Armor Penetration."
    }

    private fun setAsheAbilities() {
        binding.imageView.setImageResource(R.drawable.ashe_passive)
        binding.ability1.text = "FROST SHOT\nAshe's attacks slow their target, causing her to deal increased damage to these targets. Ashe's critical strikes deal no bonus damage but apply an empowered slow to the target."

        binding.imageView2.setImageResource(R.drawable.ashe_q)
        binding.ability2.text = "RANGER'S FOCUS\nAshe builds up Focus by attacking. At maximum Focus, Ashe can cast Ranger's Focus to consume all stacks of Focus, temporarily increasing her Attack Speed and transforming her basic attack into a powerful flurry attack for the duration."

        binding.imageView3.setImageResource(R.drawable.ashe_w)
        binding.ability3.text = "VOLLEY\nAshe fires arrows in a cone for increased damage. Also applies Frost Shot"

        binding.imageView4.setImageResource(R.drawable.ashe_e)
        binding.ability4.text = "HAWKSHOT\nAshe sends her Hawk Spirit on a scouting mission anywhere on the map."

        binding.imageView5.setImageResource(R.drawable.ashe_r)
        binding.ability5.text = "ENCHANTED CRYSTAL ARROW\nAshe fires a missile of ice in a straight line. If the arrow collides with an enemy Champion, it deals damage and stuns the Champion, stunning for longer the farther arrow has traveled. In addition, surrounding enemy units take damage and are slowed."
    }

    private fun setMissFortuneAbilities() {
        binding.imageView.setImageResource(R.drawable.miss_fortune_passive)
        binding.ability1.text = "LOVE TAP\nMiss Fortune deals bonus physical damage whenever she basic attacks a new target."

        binding.imageView2.setImageResource(R.drawable.miss_fortune_q)
        binding.ability2.text = "DOUBLE UP\nMiss Fortune fires a bullet at an enemy, damaging them and a target behind them. Both strikes can also apply Love Tap."

        binding.imageView3.setImageResource(R.drawable.miss_fortune_w)
        binding.ability3.text = "STRUT\nMiss Fortune passively gains Move Speed when not attacked. This ability can be activated to grant bonus Attack Speed for a short duration. While it's on cooldown, Love Taps reduce the remaining cooldown of Strut."

        binding.imageView4.setImageResource(R.drawable.miss_fortune_e)
        binding.ability4.text = "MAKE IT RAIN\nMiss Fortune reveals an area with a flurry of bullets, dealing waves of damage to opponents and slowing them."

        binding.imageView5.setImageResource(R.drawable.miss_fortune_r)
        binding.ability5.text = "BULLET TIME\nMiss Fortune channels a barrage of bullets into a cone in front of her, dealing large amounts of damage to enemies. Each wave of Bullet Time can critically strike"
    }

    private fun setDariusAbilities() {
        binding.imageView.setImageResource(R.drawable.darius_passive)
        binding.ability1.text = "HEMORRHAGE\nDarius' attacks and damaging abilities cause enemies to bleed for physical damage over 5 seconds, stacking up to 5 times. Darius enrages and gains massive Attack Damage when his target reaches max stacks."

        binding.imageView2.setImageResource(R.drawable.darius_q)
        binding.ability2.text = "DECIMATE\nDarius winds up and swings his axe in a wide circle. Enemies struck by the blade take more damage than those struck by the handle. Darius heals based on enemy champions and large monsters hit by the blade."

        binding.imageView3.setImageResource(R.drawable.darius_w)
        binding.ability3.text = "CRIPPLING STRIKE\nDarius's next attack strikes an enemy's crucial artery. As they bleed out, their Move Speed is slowed."

        binding.imageView4.setImageResource(R.drawable.darius_e)
        binding.ability4.text = "APPREHEND\nDarius hones his axe, passively causing his physical damage to ignore a percentage of his target's Armor. When activated, Darius sweeps up his enemies with his axe's hook and pulls them to him."

        binding.imageView5.setImageResource(R.drawable.darius_r)
        binding.ability5.text = "NOXIAN GUILLOTINE\nDarius leaps to an enemy champion and strikes a lethal blow, dealing true damage. This damage is increased for each stack of Hemorrhage on the target. If Noxian Guillotine is a killing blow, its cooldown is refreshed for a brief duration.\n"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}