package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentMainMenuBinding

class MainMenuFragment : Fragment() {
    private var _binding: FragmentMainMenuBinding? = null
    private val binding: FragmentMainMenuBinding
        get() = _binding!!

    private var btnChampClicked = false
    private var btnOptionsClicked = false
    private var btnExitClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainMenuBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val musicEnabled = sharedPreferences.getBoolean("music_enabled", true)
        val soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        if(musicEnabled){
            MAIN.mediaPlayer.start()
        }
        binding.champBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnChampClicked){
                btnChampClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_mainMenuFragment_to_choiceFragment)
                }, 200)
            }
        }
        binding.optionsBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnOptionsClicked){
                btnOptionsClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_mainMenuFragment_to_optionsFragment)
                }, 200)
            }
        }
        binding.exitBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnExitClicked){
                btnExitClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_mainMenuFragment_to_exitFragment)
                }, 200)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}