package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentChampionBinding
import com.champions.destiny.app.databinding.FragmentChoiceBinding

class ChampionFragment : Fragment() {

    private var _binding: FragmentChampionBinding? = null
    private val binding: FragmentChampionBinding
        get() = _binding!!
    private var btnBackClicked = false
    private var btnMenuClicked = false
    private var btnNextClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChampionBinding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                MAIN.navController.navigate(R.id.action_championFragment_to_choiceFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        binding.backBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnBackClicked){
                btnBackClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_championFragment_to_choiceFragment)
                }, 200)
            }
        }
        binding.menuBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnMenuClicked){
                btnMenuClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_championFragment_to_mainMenuFragment)
                }, 200)
            }
        }
        binding.nextBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnNextClicked){
                btnNextClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_championFragment_to_abilitiesFragment)
                }, 200)
            }
        }
        val champion = sharedPreferences.getString("champion", "true")
        when (champion) {
            "jinx" -> {
                setJinxAbilities()
            }
            "garen" -> {
                setGarenAbilities()
            }
            "katarina" -> {
                setKatarinaAbilities()
            }
            "yasuo" -> {
                setYasuoAbilities()
            }
            "ashe" -> {
                setAsheAbilities()
            }
            "miss_fortune" -> {
                setMissFortuneAbilities()
            }
            "darius" -> {
                setDariusAbilities()
            }
        }
    }

    private fun setJinxAbilities(){
        binding.textView2.text = "THE LOOSE\nCANNON"
        binding.nameTxt.text = "JINX"
        binding.img.setImageResource(R.drawable.jinx)
    }

    private fun setGarenAbilities(){
        binding.textView2.text = "THE MIGHT OF\nDEMACIA"
        binding.nameTxt.text = "GAREN"
        binding.img.setImageResource(R.drawable.garen)
    }
    private fun setKatarinaAbilities(){
        binding.textView2.text = "THE SINISTER\nBLADE"
        binding.nameTxt.text = "KATARINA"
        binding.img.setImageResource(R.drawable.katarina)
    }
    private fun setYasuoAbilities(){
        binding.textView2.text = "THE\nUNFORGIVEN"
        binding.nameTxt.text = "YASUO"
        binding.img.setImageResource(R.drawable.yasuo)
    }
    private fun setAsheAbilities(){
        binding.textView2.text = "THE FROST\nARCHER"
        binding.nameTxt.text = "ASHE"
        binding.img.setImageResource(R.drawable.ashe)
    }
    private fun setMissFortuneAbilities(){
        binding.textView2.text = "THE BOUNTY\nHUNTER"
        binding.nameTxt.text = "MISS FORTUNE"
        binding.nameTxt.textSize = 34f
        binding.img.setImageResource(R.drawable.miss_fortune)
    }
    private fun setDariusAbilities(){
        binding.textView2.text = "THE HAND OF\nNOXUS"
        binding.nameTxt.text = "DARIUS"
        binding.img.setImageResource(R.drawable.darius)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}