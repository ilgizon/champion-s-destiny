package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentOptionsBinding
import kotlin.properties.Delegates

class OptionsFragment : Fragment() {
    private var _binding: FragmentOptionsBinding? = null
    private val binding: FragmentOptionsBinding
        get() = _binding!!
    private var soundIsChecked = true
    private var musicIsChecked = true
    private var btnBackClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOptionsBinding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                savePrefs()
                MAIN.navController.navigate(R.id.action_optionsFragment_to_mainMenuFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)
        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        musicIsChecked = sharedPreferences.getBoolean("music_enabled", true)

        binding.backBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            savePrefs()
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnBackClicked){
                btnBackClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_optionsFragment_to_mainMenuFragment)
                }, 200)
            }
        }

        if(soundIsChecked){
            binding.soundBtn.setImageResource(R.drawable.check)
        } else {
            binding.soundBtn.setImageResource(R.drawable.uncheck)
        }
        if(musicIsChecked){
            binding.musicBtn.setImageResource(R.drawable.check)
        } else {
            binding.musicBtn.setImageResource(R.drawable.uncheck)
        }

        binding.soundTxt.setOnClickListener {
            if(soundIsChecked){
                binding.soundBtn.setImageResource(R.drawable.uncheck)
                soundIsChecked = false
            } else {
                binding.soundBtn.setImageResource(R.drawable.check)
                soundIsChecked = true
            }
        }
        binding.soundBtn.setOnClickListener {
            if(soundIsChecked){
                binding.soundBtn.setImageResource(R.drawable.uncheck)
                soundIsChecked = false
            } else {
                binding.soundBtn.setImageResource(R.drawable.check)
                soundIsChecked = true
            }
        }

        binding.musicTxt.setOnClickListener {
            if(musicIsChecked){
                binding.musicBtn.setImageResource(R.drawable.uncheck)
                musicIsChecked = false
                MAIN.mediaPlayer.pause()
            } else {
                binding.musicBtn.setImageResource(R.drawable.check)
                musicIsChecked = true
                MAIN.mediaPlayer.start()
            }
        }
        binding.musicBtn.setOnClickListener {
            if(musicIsChecked){
                binding.musicBtn.setImageResource(R.drawable.uncheck)
                musicIsChecked = false
                MAIN.mediaPlayer.pause()
            } else {
                binding.musicBtn.setImageResource(R.drawable.check)
                musicIsChecked = true
                MAIN.mediaPlayer.start()
            }
        }
    }

    private fun savePrefs(){
        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        with(sharedPreferences.edit()) {
            putBoolean("sound_enabled", soundIsChecked)
            putBoolean("music_enabled",musicIsChecked)
            apply()
        }
    }

    override fun onPause() {
        super.onPause()
        savePrefs()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}