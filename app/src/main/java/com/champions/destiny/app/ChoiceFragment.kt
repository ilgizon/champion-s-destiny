package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentChoiceBinding
import com.champions.destiny.app.databinding.FragmentMainMenuBinding

class ChoiceFragment : Fragment() {

    private var _binding: FragmentChoiceBinding? = null
    private val binding: FragmentChoiceBinding
        get() = _binding!!
    private var btnBackClicked = false
    private var btnMenuClicked = false
    private var btnJinxClicked = false
    private var btnGarenClicked = false
    private var btnKatarinaClicked = false
    private var btnYasuoClicked = false
    private var btnAsheClicked = false
    private var btnMissFortuneClicked = false
    private var btnDariusClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentChoiceBinding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                MAIN.navController.navigate(R.id.action_choiceFragment_to_mainMenuFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        binding.backBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnBackClicked){
                btnBackClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_mainMenuFragment)
                }, 200)
            }
        }
        binding.menuBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnMenuClicked){
                btnMenuClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_mainMenuFragment)
                }, 200)
            }
        }
        binding.jinxBtn.setOnClickListener {
            savePrefs("jinx")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnJinxClicked){
                btnJinxClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
        binding.garenBtn.setOnClickListener {
            savePrefs("garen")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnGarenClicked){
                btnGarenClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
        binding.katarinaBtn.setOnClickListener {
            savePrefs("katarina")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnKatarinaClicked){
                btnKatarinaClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
        binding.yasuoBtn.setOnClickListener {
            savePrefs("yasuo")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnYasuoClicked){
                btnYasuoClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
        binding.asheBtn.setOnClickListener {
            savePrefs("ashe")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnAsheClicked){
                btnAsheClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
        binding.missFortuneBtn.setOnClickListener {
            savePrefs("miss_fortune")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnMissFortuneClicked){
                btnMissFortuneClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
        binding.dariusBtn.setOnClickListener {
            savePrefs("darius")
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnDariusClicked){
                btnDariusClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_choiceFragment_to_championFragment)
                }, 200)
            }
        }
    }

    private fun savePrefs(champion: String){
        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        with(sharedPreferences.edit()) {
            putString("champion", champion)
            apply()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}