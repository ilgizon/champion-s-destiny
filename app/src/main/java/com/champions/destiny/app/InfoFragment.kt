package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentChampionBinding
import com.champions.destiny.app.databinding.FragmentInfoBinding

class InfoFragment : Fragment() {

    private var _binding: FragmentInfoBinding? = null
    private val binding: FragmentInfoBinding
        get() = _binding!!
    private var btnBackClicked = false
    private var btnMenuClicked = false
    private var btnNextClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInfoBinding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                MAIN.navController.navigate(R.id.action_infoFragment_to_abilitiesFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        binding.backBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnBackClicked){
                btnBackClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_infoFragment_to_abilitiesFragment)
                }, 200)
            }
        }
        binding.menuBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnMenuClicked){
                btnMenuClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_infoFragment_to_mainMenuFragment)
                }, 200)
            }
        }
        binding.nextBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnNextClicked){
                btnNextClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_infoFragment_to_choiceFragment)
                }, 200)
            }
        }
        val champion = sharedPreferences.getString("champion", "true")
        when (champion) {
            "jinx" -> {
                setJinxAbilities()
            }
            "garen" -> {
                setGarenAbilities()
            }
            "katarina" -> {
                setKatarinaAbilities()
            }
            "yasuo" -> {
                setYasuoAbilities()
            }
            "ashe" -> {
                setAsheAbilities()
            }
            "miss_fortune" -> {
                setMissFortuneAbilities()
            }
            "darius" -> {
                setDariusAbilities()
            }
        }
    }

    private fun setJinxAbilities() {
        binding.infoImg.setImageResource(R.drawable.jinx_info_img)
        binding.ability.text =
            "A manic and impulsive criminal from Zaun, Jinx lives to wreak havoc without care for the consequences. With an arsenal of deadly weapons, she unleashes the loudest blasts and brightest explosions to leave a trail of mayhem and panic in her wake. Jinx despises boredom, and gleefully brings her own chaotic brand of pandemonium wherever she goes."
    }

    private fun setGarenAbilities() {
        binding.infoImg.setImageResource(R.drawable.garen_info_img)
        binding.ability.text = "A proud and noble warrior, Garen fights as one of the Dauntless Vanguard. He is popular among his fellows, and respected well enough by his enemies—not least as a scion of the prestigious Crownguard family, entrusted with defending Demacia and its ideals. Clad in magic-resistant armor and bearing a mighty broadsword, Garen stands ready to confront mages and sorcerers on the field of battle, in a veritable whirlwind of righteous steel."
    }

    private fun setKatarinaAbilities() {
        binding.infoImg.setImageResource(R.drawable.katarina_info_img)
        binding.ability.text = "Decisive in judgment and lethal in combat, Katarina is a Noxian assassin of the highest caliber. Eldest daughter to the legendary General Du Couteau, she made her talents known with swift kills against unsuspecting enemies. Her fiery ambition has driven her to pursue heavily-guarded targets, even at the risk of endangering her allies—but no matter the mission, Katarina will not hesitate to execute her duty amid a whirlwind of serrated daggers."
    }

    private fun setYasuoAbilities() {
        binding.infoImg.setImageResource(R.drawable.yasuo_info_img)
        binding.ability.text = "An Ionian of deep resolve, Yasuo is an agile swordsman who wields the air itself against his enemies. As a proud young man, he was falsely accused of murdering his master—unable to prove his innocence, he was forced to slay his own brother in self defense. Even after his master's true killer was revealed, Yasuo still could not forgive himself for all he had done, and now wanders his homeland with only the wind to guide his blade."
    }

    private fun setAsheAbilities() {
        binding.infoImg.setImageResource(R.drawable.ashe_info_img)
        binding.ability.text = "Iceborn warmother of the Avarosan tribe, Ashe commands the most populous horde in the north. Stoic, intelligent, and idealistic, yet uncomfortable with her role as leader, she taps into the ancestral magics of her lineage to wield a bow of True Ice. With her people's belief that she is the mythological hero Avarosa reincarnated, Ashe hopes to unify the Freljord once more by retaking their ancient, tribal lands."
    }

    private fun setMissFortuneAbilities() {
        binding.infoImg.setImageResource(R.drawable.miss_fortune_info_img)
        binding.ability.text = "A Bilgewater captain famed for her looks but feared for her ruthlessness, Sarah Fortune paints a stark figure among the hardened criminals of the port city. As a child, she witnessed the reaver king Gangplank murder her family—an act she brutally avenged years later, blowing up his flagship while he was still aboard. Those who underestimate her will face a beguiling and unpredictable opponent… and, likely, a bullet or two in their guts."
    }

    private fun setDariusAbilities() {
        binding.infoImg.setImageResource(R.drawable.darius_info_img)
        binding.ability.text = "There is no greater symbol of Noxian might than Darius, the nation's most feared and battle-hardened commander. Rising from humble origins to become the Hand of Noxus, he cleaves through the empire's enemies—many of them Noxians themselves. Knowing that he never doubts his cause is just, and never hesitates once his axe is raised, those who stand against the leader of the Trifarian Legion can expect no mercy."
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}