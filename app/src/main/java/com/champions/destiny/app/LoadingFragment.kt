package com.champions.destiny.app

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.champions.destiny.app.databinding.FragmentLoadingBinding

class LoadingFragment : Fragment() {
    private lateinit var binding: FragmentLoadingBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoadingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rotationAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.rotate)
        binding.progressBar.startAnimation(rotationAnimation)
        Handler().postDelayed({
            MAIN.navController.navigate(R.id.action_loadingFragment_to_mainMenuFragment)
        }, 2000)

    }

}