package com.champions.destiny.app

import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation

object AnimationUtils {
    fun onButtonClick(view: View) {
        val animation = ScaleAnimation(
            1f,
            0.9f,
            1f,
            0.9f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        animation.duration = 100
        animation.repeatCount = 1
        animation.repeatMode = Animation.REVERSE
        view.startAnimation(animation)
    }
}