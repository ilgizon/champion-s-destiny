package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.champions.destiny.app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var navController: NavController
    lateinit var binding: ActivityMainBinding
    lateinit var mediaPlayer: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        setContentView(binding.root)
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        mediaPlayer = MediaPlayer.create(this, R.raw.music)
        mediaPlayer.isLooping = true
        MAIN = this
    }

    override fun onRestart() {
        super.onRestart()
        val musicEnabled = this.getPreferences(Context.MODE_PRIVATE).getBoolean("music_enabled", true)
        if(musicEnabled){
            mediaPlayer.start()
        }
    }

    override fun onStop() {
        super.onStop()
        mediaPlayer.pause()
    }

}