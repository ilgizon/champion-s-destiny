package com.champions.destiny.app

import android.content.Context
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker.OnValueChangeListener
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.champions.destiny.app.databinding.FragmentExitBinding
import com.champions.destiny.app.databinding.FragmentOptionsBinding

class ExitFragment : Fragment() {
    private var _binding: FragmentExitBinding? = null
    private val binding: FragmentExitBinding
        get() = _binding!!
    private var btnBackClicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentExitBinding.inflate(inflater, container, false)
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
               findNavController().navigate(R.id.action_exitFragment_to_mainMenuFragment)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val soundPlayer = MediaPlayer.create(requireContext(), R.raw.click)

        val sharedPreferences = requireActivity().getPreferences(Context.MODE_PRIVATE)
        val soundIsChecked = sharedPreferences.getBoolean("sound_enabled", true)
        binding.backBtn.setOnClickListener {
            AnimationUtils.onButtonClick(it)
            if(soundIsChecked){
                soundPlayer.start()
            }
            if(!btnBackClicked){
                btnBackClicked = true
                Handler().postDelayed({
                    findNavController().navigate(R.id.action_exitFragment_to_mainMenuFragment)
                }, 200)
            }
        }
        binding.yesTxt.setOnClickListener {
            requireActivity().finish()
        }
        binding.yesBtn.setOnClickListener {
            requireActivity().finish()
        }
        binding.noTxt.setOnClickListener {
            findNavController().navigate(R.id.action_exitFragment_to_mainMenuFragment)
        }
        binding.noBtn.setOnClickListener {
            findNavController().navigate(R.id.action_exitFragment_to_mainMenuFragment)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}